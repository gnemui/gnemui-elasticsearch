package com.atguigu.es.query;

import org.apache.http.HttpHost;
import org.elasticsearch.action.search.SearchRequest;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.client.RequestOptions;
import org.elasticsearch.client.RestClient;
import org.elasticsearch.client.RestHighLevelClient;
import org.elasticsearch.index.query.BoolQueryBuilder;
import org.elasticsearch.index.query.MatchQueryBuilder;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.search.SearchHit;
import org.elasticsearch.search.SearchHits;
import org.elasticsearch.search.builder.SearchSourceBuilder;

public class Query06 {

    public static void main(String[] args) throws Exception {
        // 创建ES客户端
        RestHighLevelClient esClient = new RestHighLevelClient(
                RestClient.builder(new HttpHost(
                        "localhost",
                        9200,
                        "http"
                ))
        );

        // 6. 查询索引中的数据 - 组合查询
        SearchRequest request = new SearchRequest();

        // 指定查询的索引
        request.indices("user");

        // 指定索引查询的条件 - 联合条件查询
        SearchSourceBuilder builder = new SearchSourceBuilder();

        BoolQueryBuilder boolQueryBuilder = QueryBuilders.boolQuery();

        // 条件1
        // new SearchSourceBuilder().query(QueryBuilders.boolQuery().must(QueryBuilders.matchQuery("age", 30)))
        MatchQueryBuilder matchQueryBuilder1 = QueryBuilders.matchQuery("age", 30);
        boolQueryBuilder.must(matchQueryBuilder1);
        // 条件2
        // new SearchSourceBuilder().query(QueryBuilders.boolQuery().must(QueryBuilders.matchQuery("sex", "男")))
        MatchQueryBuilder matchQueryBuilder2 = QueryBuilders.matchQuery("sex", "男");
        boolQueryBuilder.must(matchQueryBuilder2);

        // boolQueryBuilder.must(QueryBuilders.matchQuery("sex", "男"));
        // boolQueryBuilder.mustNot(QueryBuilders.matchQuery("sex", "男"));

        // boolQueryBuilder.should(QueryBuilders.matchQuery("age", 30));
        // boolQueryBuilder.should(QueryBuilders.matchQuery("age", 40));

        builder.query(boolQueryBuilder);

        request.source(builder);

        // 开始索引中数据的查询
        SearchResponse response = esClient.search(request, RequestOptions.DEFAULT);

        // 输出索引中数据的条数以及查询耗时
        SearchHits hits = response.getHits();
        System.out.println(hits.getTotalHits());
        System.out.println(response.getTook());

        // 循环输出索引中数据的内容
        for (SearchHit hit : hits) {
            System.out.println(hit.getSourceAsString());
        }

        // 关闭ES客户端
        esClient.close();
    }

}
