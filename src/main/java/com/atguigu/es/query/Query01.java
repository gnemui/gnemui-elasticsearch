package com.atguigu.es.query;

import org.apache.http.HttpHost;
import org.elasticsearch.action.search.SearchRequest;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.client.RequestOptions;
import org.elasticsearch.client.RestClient;
import org.elasticsearch.client.RestHighLevelClient;
import org.elasticsearch.index.query.*;
import org.elasticsearch.search.SearchHit;
import org.elasticsearch.search.SearchHits;
import org.elasticsearch.search.builder.SearchSourceBuilder;
import org.elasticsearch.search.sort.SortOrder;

public class Query01 {

    public static void main(String[] args) throws Exception {
        // 创建ES客户端
        RestHighLevelClient esClient = new RestHighLevelClient(
                RestClient.builder(new HttpHost(
                        "localhost",
                        9200,
                        "http"
                ))
        );

        // 1. 查询索引中全部的数据
        SearchRequest request = new SearchRequest();

        // 指定查询的索引
        request.indices("user");

        // 指定索引查询的条件 - 查询所有
        MatchAllQueryBuilder matchAllQueryBuilder = QueryBuilders.matchAllQuery();
        SearchSourceBuilder searchSourceBuilder = new SearchSourceBuilder();
        SearchSourceBuilder query = searchSourceBuilder.query(matchAllQueryBuilder);
        // new SearchSourceBuilder().query(QueryBuilders.matchAllQuery())
        request.source(query);

        // 开始索引中数据的查询
        SearchResponse response = esClient.search(request, RequestOptions.DEFAULT);

        // 输出索引中数据的条数以及查询耗时
        SearchHits hits = response.getHits();
        System.out.println(hits.getTotalHits());
        System.out.println(response.getTook());

        // 循环输出索引中数据的内容
        for (SearchHit hit : hits) {
            System.out.println(hit.getSourceAsString());
        }

        // 关闭ES客户端
        esClient.close();
    }

}
