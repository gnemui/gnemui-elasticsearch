package com.atguigu.es.query;

import org.apache.http.HttpHost;
import org.elasticsearch.action.search.SearchRequest;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.client.RequestOptions;
import org.elasticsearch.client.RestClient;
import org.elasticsearch.client.RestHighLevelClient;
import org.elasticsearch.common.unit.Fuzziness;
import org.elasticsearch.index.query.FuzzyQueryBuilder;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.search.SearchHit;
import org.elasticsearch.search.SearchHits;
import org.elasticsearch.search.builder.SearchSourceBuilder;

public class Query08 {

    public static void main(String[] args) throws Exception {
        // 创建ES客户端
        RestHighLevelClient esClient = new RestHighLevelClient(
                RestClient.builder(new HttpHost(
                        "localhost",
                        9200,
                        "http"
                ))
        );

        // 8. 查询索引中的数据 - 模糊查询
        SearchRequest request = new SearchRequest();

        // 指定查询的索引
        request.indices("user");

        // 指定索引查询的条件 - 模糊查询
        SearchSourceBuilder builder = new SearchSourceBuilder();

        FuzzyQueryBuilder fuzzyQueryBuilder = QueryBuilders.fuzzyQuery("name", "wangwu");
        // 模糊的程度 - 允许差异的字符个数
        fuzzyQueryBuilder.fuzziness(Fuzziness.ONE);

        // QueryBuilders.fuzzyQuery("name", "wangwu").fuzziness(Fuzziness.ZERO); // 精准匹配
        // QueryBuilders.fuzzyQuery("name", "wangwu").fuzziness(Fuzziness.ONE); // 允许一个字符的差别
        // QueryBuilders.fuzzyQuery("name", "wangwu").fuzziness(Fuzziness.TWO); // 允许两个以内的字符的差别

        builder.query(fuzzyQueryBuilder);

        request.source(builder);

        // 开始索引中数据的查询
        SearchResponse response = esClient.search(request, RequestOptions.DEFAULT);

        // 输出索引中数据的条数以及查询耗时
        SearchHits hits = response.getHits();
        System.out.println(hits.getTotalHits());
        System.out.println(response.getTook());

        // 循环输出索引中数据的内容
        for (SearchHit hit : hits) {
            System.out.println(hit.getSourceAsString());
        }

        // 关闭ES客户端
        esClient.close();
    }

}
