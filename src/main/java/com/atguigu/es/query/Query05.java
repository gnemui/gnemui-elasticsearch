package com.atguigu.es.query;

import org.apache.http.HttpHost;
import org.elasticsearch.action.search.SearchRequest;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.client.RequestOptions;
import org.elasticsearch.client.RestClient;
import org.elasticsearch.client.RestHighLevelClient;
import org.elasticsearch.index.query.MatchAllQueryBuilder;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.search.SearchHit;
import org.elasticsearch.search.SearchHits;
import org.elasticsearch.search.builder.SearchSourceBuilder;

public class Query05 {

    public static void main(String[] args) throws Exception {
        // 创建ES客户端
        RestHighLevelClient esClient = new RestHighLevelClient(
                RestClient.builder(new HttpHost(
                        "localhost",
                        9200,
                        "http"
                ))
        );

        // 5. 查询索引中的数据 - 查询字段过滤
        SearchRequest request = new SearchRequest();

        // 指定查询的索引
        request.indices("user");

        // 指定索引查询的条件 - 先全量查询然后进行字段过滤
        MatchAllQueryBuilder matchAllQueryBuilder = QueryBuilders.matchAllQuery();
        SearchSourceBuilder searchSourceBuilder = new SearchSourceBuilder();
        SearchSourceBuilder query = searchSourceBuilder.query(matchAllQueryBuilder);

        String[] includes = {"name"};
        String[] excludes = {"age"};
        query.fetchSource(includes, excludes);

        // new SearchSourceBuilder().query(QueryBuilders.matchAllQuery()).fetchSource(includes, excludes)
        request.source(query);

        // 开始索引中数据的查询
        SearchResponse response = esClient.search(request, RequestOptions.DEFAULT);

        // 输出索引中数据的条数以及查询耗时
        SearchHits hits = response.getHits();
        System.out.println(hits.getTotalHits());
        System.out.println(response.getTook());

        // 循环输出索引中数据的内容
        for (SearchHit hit : hits) {
            System.out.println(hit.getSourceAsString());
        }

        // 关闭ES客户端
        esClient.close();
    }

}
