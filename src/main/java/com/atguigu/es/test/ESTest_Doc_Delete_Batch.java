package com.atguigu.es.test;

import org.apache.http.HttpHost;
import org.elasticsearch.action.bulk.BulkRequest;
import org.elasticsearch.action.bulk.BulkResponse;
import org.elasticsearch.action.delete.DeleteRequest;
import org.elasticsearch.action.delete.DeleteResponse;
import org.elasticsearch.client.RequestOptions;
import org.elasticsearch.client.RestClient;
import org.elasticsearch.client.RestHighLevelClient;

public class ESTest_Doc_Delete_Batch {

    public static void main(String[] args) throws Exception {
        // 创建ES客户端
        RestHighLevelClient esClient = new RestHighLevelClient(
                RestClient.builder(new HttpHost(
                        "localhost",
                        9200,
                        "http"
                ))
        );

        // 批量删除文档数据
        BulkRequest request = new BulkRequest();

        // 构造单个删除
        DeleteRequest request1 = new DeleteRequest();
        request1.index("user").id("1001");
        DeleteRequest request2 = new DeleteRequest();
        request2.index("user").id("1002");
        DeleteRequest request3 = new DeleteRequest();
        request3.index("user").id("1003");

        // 构造删除批量组
        request.add(request1);
        request.add(request2);
        request.add(request3);

        // 开始批量删除
        BulkResponse response = esClient.bulk(request, RequestOptions.DEFAULT);

        // 输出操作结果
        System.out.println(response.getTook());
        System.out.println(response.getItems());

        // 关闭ES客户端
        esClient.close();
    }

}
