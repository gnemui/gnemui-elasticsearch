package com.atguigu.es.test;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.http.HttpHost;
import org.elasticsearch.action.bulk.BulkRequest;
import org.elasticsearch.action.bulk.BulkResponse;
import org.elasticsearch.action.index.IndexRequest;
import org.elasticsearch.action.index.IndexResponse;
import org.elasticsearch.client.RequestOptions;
import org.elasticsearch.client.RestClient;
import org.elasticsearch.client.RestHighLevelClient;
import org.elasticsearch.common.xcontent.XContentType;

public class ESTest_Doc_Insert_Batch {

    public static void main(String[] args) throws Exception {
        // 创建ES客户端
        RestHighLevelClient esClient = new RestHighLevelClient(
                RestClient.builder(new HttpHost(
                        "localhost",
                        9200,
                        "http"
                ))
        );

        // 批量插入数据
        BulkRequest request = new BulkRequest();

        // 构造单个request
        IndexRequest request1 = new IndexRequest();
        request1.index("user").id("1001")
                .source(XContentType.JSON, "name", "zhangshan", "sex", "男", "age", 30);

        IndexRequest request2 = new IndexRequest();
        request2.index("user").id("1002")
                .source(XContentType.JSON, "name", "lisi", "sex", "女", "age", 30);

        IndexRequest request3 = new IndexRequest();
        request3.index("user").id("1003")
                .source(XContentType.JSON, "name", "wangwu", "sex", "男", "age", 40);

        IndexRequest request4 = new IndexRequest();
        request4.index("user").id("1004")
                .source(XContentType.JSON, "name", "wangwu1", "sex", "女", "age", 40);

        IndexRequest request5 = new IndexRequest();
        request5.index("user").id("1005")
                .source(XContentType.JSON, "name", "wangwu2", "sex", "男", "age", 50);

        IndexRequest request6 = new IndexRequest();
        request6.index("user").id("1006")
                .source(XContentType.JSON, "name", "wangwu3", "sex", "男", "age", 50);

        // 添加多个单数据进入集合
        request.add(request1);
        request.add(request2);
        request.add(request3);
        request.add(request4);
        request.add(request5);
        request.add(request6);

        // 批量添加
        BulkResponse response = esClient.bulk(request, RequestOptions.DEFAULT);

        // 输出操作结果
        System.out.println(response.getTook());
        System.out.println(response.getItems());

        // 关闭ES客户端
        esClient.close();
    }

}
